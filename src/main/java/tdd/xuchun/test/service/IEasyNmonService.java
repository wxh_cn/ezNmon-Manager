package tdd.xuchun.test.service;

import java.util.List;

import tdd.xuchun.test.model.Host;

public interface IEasyNmonService {

	// 上传并启动easyNmon
	public boolean upAndRun(Host host, int cover);

	// 发送指令
	public int accessTheURL(Host host, String urlSuffix);

	// 清除报告
	public boolean clearReport(Host host);

	// 报告打包并下载
	public boolean getReportPackage(Host host);

	// 查询easyNmon包中的nmon文件支持的操作系统

	public List<String> getEzNmonOS();

	// 清除服务器上的easynmon服务
	public boolean clearService(Host host);
}
